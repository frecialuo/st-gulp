'use strict';

(function () {
    var arr = Array.from({
        length: 10
    }, function (val, idx) {
        return idx++;
    });
    console.log(arr.join(' '));
})();