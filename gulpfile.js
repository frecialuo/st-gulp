/**
 * [引入套件]
 * 需先安裝(全域) gulp-cli 以便在 terminal 使用 gulp
 * 看到 cli 字樣的套件, 就是跟 command 有關
 */

/** [gulp] */
const gulp = require('gulp');

/** [html plugin] */
const htmlReplace = require('gulp-html-replace'); // 修正靜態資源的輸出路徑

/** [css plugin] */
const sass = require('gulp-sass'); // css 預處理器
const autoprefixer = require('gulp-autoprefixer'); // css 自動加前綴

/** [js plugin] */
const babel = require('gulp-babel'); // js 編譯
const concat = require('gulp-concat'); // js 合併
const uglify = require('gulp-uglify'); // js 壓縮
const sourcemaps = require('gulp-sourcemaps'); // 開發偵錯用

/** [tools] */
const del = require('del'); // 刪除文件、檔案
const browserSync = require('browser-sync').create(); // 瀏覽器同步顯示
const SSI = require('browsersync-ssi');

/** [輸入輸出目錄] */
const dirs = {
    src: 'src',
    dest: 'dist'
};

/** [html 路徑] */
const htmlPath = {
    src: dirs.src + '/*.html',
    dest: dirs.dest + '/'
}

/** [css 路徑] */
const stylePath = {
    src: dirs.src + '/scss/*.scss',
    dest: dirs.dest + '/css'
};

/** [js 路徑] */
const scriptPath = {
    src: dirs.src + '/js/*.js',
    dest: dirs.dest + '/js'
};

/** [整合常數] */
const file = {
    css: 'bundle.css',
    js: 'bundle.js'
};

/** [處理 html 任務] */
gulp.task('html', function () {
    return gulp.src(htmlPath.src)
        .pipe(htmlReplace({
            'css': 'css/' + file.css,
            'js': 'js/' + file.js
        }))
        .pipe(gulp.dest(htmlPath.dest)) // 輸出路徑
        .pipe(browserSync.stream()); // 瀏覽器同步
});

/** [處理 css 任務] */
gulp.task('style', function () {
    return gulp.src(stylePath.src)
        .pipe(sass({
            outputStyle: 'expanded'
        })).on('error', sass.logError)
        .pipe(autoprefixer({
            browsers: ['last 5 versions'], // 支援瀏覽器版本
            cascade: true, // 美化屬性值 默認:true, like as
            // -webkit-transform: rotate(45deg);
            //         transform: rotate(45deg);
            remove: true // 去掉不必要的前綴 默認:true
        }))
        .pipe(concat(file.css)) // 合併成一支 bundle.css
        .pipe(gulp.dest(stylePath.dest)) // 輸出路徑
        .pipe(browserSync.stream()); // 瀏覽器同步
});

/** [處理 js 任務] */
gulp.task('script', function () {
    return gulp.src(scriptPath.src)
        .pipe(babel()) // es6 > es5
        .pipe(concat(file.js)) // 合併成一支 bundle.js
        // .pipe(uglify()) // 壓縮
        .pipe(gulp.dest(scriptPath.dest)) // 輸出路徑
        .pipe(browserSync.stream()); // 瀏覽器同步
});

/** [監看目錄] */
gulp.task('watch', function () {
    gulp.watch(htmlPath.src, ['html']); // 監看 html 檔案，檔案有更動時就執行 task html
    gulp.watch(stylePath.src, ['style']); // 監看 scss 檔案，檔案有更動時就執行 task style
    gulp.watch(scriptPath.src, ['script']); // 監看 js 檔案，檔案有更動時就執行 task script
});

/** [瀏覽器同步顯示] */
gulp.task('browserSync', function () {
    browserSync.init({
        // 各項設定
        server: {
            baseDir: [dirs.dest], // 要瀏覽器同步的目錄
            middleware: SSI({
                baseDir: dirs.dest,
                ext: '.shtml',
            })
        },
        port: 3003 // 預設 3000 port
    });
});

/** [刪除文件] */
gulp.task('del', function () {
    return del([dirs.dest]);
});

/** [客製化要執行的任務列] */
gulp.task('default', ['html', 'style', 'script', 'browserSync', 'watch']);
gulp.task('build', ['html', 'style', 'script']);