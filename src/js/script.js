(function () {
    const arr = Array.from({
        length: 10
    }, (val, idx) => idx++);
    console.log(arr.join(' '));
})();